package model.data_structures;

public class Vertice<K, V, A> implements Comparable<Vertice>{

	/**
	 * Lista de arcos que dispone el v�rtice
	 */
	private Bag<Arco> arcos;


	/**
	 * Lista de v�rtices adyacentes a este
	 */
	private Bag<K> adj;

	/**
	 * Identificador �nico del v�rtice
	 */
	private K id;


	/**
	 * Indicador de si ya fue recorrido
	 */
	private boolean marca;

	/**
	 * Informaci�n de un v�rtice
	 */
	private V info;

	/**
	 * Latitud del v�rtice
	 */
	private V latitud;

	/**
	 * Latitud del v�rtice
	 */
	private V longitud;

	/**
	 * Cantidad de infracciones asociadas
	 */
	private int numInf;

	private int pesoVert;


	public Vertice(K pId, V infoVertice) {

		info = infoVertice;
		arcos = new Bag<Arco>();
		adj = new Bag<K>();
		id = pId;
		marca = false;
		numInf = 0;
		pesoVert=0;
		
		//Cambios (asignacion de lat y lon):
		
		String infoString = (String) infoVertice;
		String[] infoFrac = infoString.split("|");
		
		longitud = (V) infoFrac[0];
		latitud = (V) infoFrac[1];
		

	}


	public Bag<Arco> getArcos() {

		return arcos;

	}

	public boolean hayArcos()
	{
		boolean hay = false;
		if(getArcos().isEmpty())
		{
			
			hay= false;
		}else
		{
			
			hay = true;
		}

		return hay;
	}

	public Bag<K> getAdj() {

		return adj;

	}


	public void addEdge(Arco pArco) {

		arcos.add(pArco);

	}


	public void addAdj(K pAdj) {

		adj.add(pAdj);

	}


	public V getInfo() {

		return info;

	}

	@SuppressWarnings("unchecked")
	public void setInfoJunta()
	{
		info = (V)(getLatitud()+"|"+getLongitud());
	}

	public V getLatitud() {

		return latitud;

	}

	public void setLat(V lat)
	{
		latitud = lat;
	}

	public V getLongitud() {

		return longitud;

	}

	public void setLon(V lon) 
	{
		longitud = lon;
	}

	public K getId() {

		return id;

	}

	public void setId(K pId)
	{
		id=pId;
	}

	public boolean getMarca() {

		return marca;

	}


	public void setInfo(V pInfo) {

		info = pInfo;

	}	


	public void setNumInf(int pNumInf) {

		numInf = pNumInf;

	}


	public int getNumInf()
	{

		return numInf;

	}
	
	public void setPesoVer(int pPesoVert) {

		pesoVert = pPesoVert;

	}


	public int getPesoVert()
	{

		return pesoVert;

	}

	@Override
	public int compareTo(Vertice o) {
		// TODO Auto-generated method stub
		
		int resp = -100;
			
		resp = numInf - o.getNumInf();
		
		return resp;
	}

}
