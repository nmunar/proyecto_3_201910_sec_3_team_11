package model.data_structures;
import java.util.Iterator;
public class Queue<T> implements IQueue<T>
{
	private Node<T> primero;
	private Node<T> ultimo;
	private Node<T> actual;
	private int longitud;

	public Queue() 
	{ 
		int longitud = 0 ;
		primero = null ;
		ultimo = null;
		actual=primero;
	} 

	@Override
	public Iterator iterator() {
		return new IteratorSQ<T>(primero);
	}

	@Override
	public boolean isEmpty() 
	{ 
		return primero==null;
	}

	@Override
	public int size() 
	{
		return longitud;
	}

	@Override
	public void enqueue(T t) 
	{  
		Node<T> agregado = new Node<T>((T) t);
		if(primero==null)
		{  primero =  agregado;
		ultimo= agregado;
		longitud++;
		}else{
			agregado.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(agregado);
			ultimo = agregado;
			longitud++;
		} 
	}

	public Node<T> darUltimo()
	{
		return ultimo;
	}

	public Node<T> darPrimero()
	{
		return primero;
	}
	public Node<T> darActual()
	{
		return actual;
	}

	@Override
	public T dequeue() 
	{  
		Node<T> actual = primero;
		Node<T> siguiente = actual.darSiguiente();
		if(primero==null)
		{ }else  { 
			try {
			siguiente.cambiarAnterior(null);
			}catch (Exception e) {
				
			}
			primero = siguiente;
			longitud--;
			return actual.darElemento();
		}
		
		return actual.darElemento();

	}

}
