package model.data_structures;

public class Arco<K, A> {


	/**
	 * Información del arco
	 */
	private A info;


	/**
	 * Id del vértice de inicio
	 */
	private K v1;


	/**
	 * Id del vértice de fin
	 */
	private K v2;



	public Arco(K pV1, K pV2, A pInfo) {

		v1 = pV1;
		v2 = pV2;
		info = pInfo;

	}


	public K getV1() {

		return v1;

	}

	public K getV2() {

		return v2;

	}

	public A getInfo() {

		return info;

	}


	public void setInfo(A pInfoArc) {

		info = pInfoArc;

	}

}
