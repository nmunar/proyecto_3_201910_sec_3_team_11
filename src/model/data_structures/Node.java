package model.data_structures;

public class Node<T> {


	private T elemento;
	
	private Node<T> siguiente;

	private Node<T> anterior;

	public Node(T pElemento) {
		
		elemento = pElemento;
		anterior = null;
		siguiente = null;
		
	}


	public Node<T> darSiguiente() {

		return siguiente;

	}

	public Node<T> darAnterior()
	{

		return anterior;

	}


	public void cambiarSiguiente(Node<T> pSiguiente) {

		siguiente = pSiguiente;

	}

	public void cambiarAnterior(Node<T> pAnterior) {

		anterior = pAnterior;

	}
	
	public T darElemento() {
		
		return elemento;
		
	}
	

}