package shortestPaths;

import java.util.Iterator;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.Vertice;
import model.data_structures.hashTableLinearProbing;

/**
 *  The {@code DepthFirstSearch} class represents a data type for 
 *  determining the vertices connected to a given source vertex <em>s</em>
 *  in an undirected graph. For versions that find the paths, see
 *  {@link DepthFirstPaths} and {@link BreadthFirstPaths}.
 *  <p>
 *  This implementation uses depth-first search.
 *  See {@link NonrecursiveDFS} for a non-recursive version.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>
 *  (in the worst case),
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  It uses extra space (not including the graph) proportional to <em>V</em>.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>   
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class DepthFirstSearch {
    private boolean[] marked;    // marked[v] = is there an s-v path?
    private int count;           // number of vertices connected to s
    
    private hashTableLinearProbing<Integer, String> lpV = new hashTableLinearProbing<Integer, String>();
    private hashTableLinearProbing<String, Integer> lpV2 = new hashTableLinearProbing<String, Integer>();
    
    /**
     * Computes the vertices in graph {@code G} that are
     * connected to the source vertex {@code s}.
     * @param G the graph
     * @param s the source vertex
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */

    public DepthFirstSearch(Graph G) {
        marked = new boolean[G.getNumVertices()];
        
        int contM = 0;
        while(contM < marked.length) {
        	
        	marked[contM] = false;
        	contM++;
        }
          
        
        Iterator<String> itIdV = G.idVertices().iterator();
		int contV = 0;
		while(itIdV.hasNext()) {

			String idActual = itIdV.next();
			lpV.put(contV, idActual);
			lpV2.put(idActual, contV);

			contV++;
			
		}
		//validateVertex(s);
//        dfs(G);
    }

    // depth first search from v

        
        

//    private void dfs(Graph G) {
//        
//        	Iterator<String> act = G.getVertices().keys().iterator();
// 
//        //System.out.println(pos);
//        
//        
//        marked[pos] = true;
//    		
//    		
//    		Iterator<Vertice> it = act.getAdj().iterator();
//    		
//    		while(it.hasNext()) {
//    			
//    			Arco a = it.next();
//    			String key = (String) a.getV2();
//    			int w = lpV2.get(key);
//    			
//    			if (!marked[w]) {
//                    dfs(G, key);
//                }
//    	
//    			
//    		}
//    }

    /**
     * Is there a path between the source vertex {@code s} and vertex {@code v}?
     * @param v the vertex
     * @return {@code true} if there is a path, {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean marked(String v) {
        validateVertex(v);
        
        int pos = lpV2.get(v);
        
        return marked[pos];
    }

    /**
     * Returns the number of vertices connected to the source vertex {@code s}.
     * @return the number of vertices connected to the source vertex {@code s}
     */
    public int count() {
        return count;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(String v) {
        int V = marked.length;
        int pos = lpV2.get(v);
        
        if (pos < 0 || pos >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

//    /**
//     * Unit tests the {@code DepthFirstSearch} data type.
//     *
//     * @param args the command-line arguments
//     */
//    public static void main(String[] args) {
//        In in = new In(args[0]);
//        Graph G = new Graph(in);
//        int s = Integer.parseInt(args[1]);
//        DepthFirstSearch search = new DepthFirstSearch(G, s);
//        for (int v = 0; v < G.V(); v++) {
//            if (search.marked(v))
//                StdOut.print(v + " ");
//        }
//
//        StdOut.println();
//        if (search.count() != G.V()) StdOut.println("NOT connected");
//        else                         StdOut.println("connected");
//    }

}