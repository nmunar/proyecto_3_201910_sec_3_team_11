package shortestPaths;

import java.util.Iterator;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.IndexMinPQ;
import model.data_structures.Stack;
import model.data_structures.Vertice;
import model.data_structures.hashTableLinearProbing;

/**
 *  The {@code DijkstraSP} class represents a data type for solving the
 *  single-source shortest paths problem in edge-weighted digraphs
 *  where the edge weights are nonnegative.
 *  <p>
 *  This implementation uses Dijkstra's algorithm with a binary heap.
 *  The constructor takes time proportional to <em>E</em> log <em>V</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Each call to {@code distTo(int)} and {@code hasPathTo(int)} takes constant time;
 *  each call to {@code pathTo(int)} takes time proportional to the number of
 *  edges in the shortest path returned.
 *  <p>
 *  For additional documentation,    
 *  see <a href="https://algs4.cs.princeton.edu/44sp">Section 4.4</a> of    
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne. 
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class DijkstraSP {
	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
	private Arco[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices



	private hashTableLinearProbing<Integer, String> lpV;
	private hashTableLinearProbing<String, Integer> lpV2;
	
	
	String numPathTo = "";

	/**
	 * Computes a shortest-paths tree from the source vertex {@code s} to every other
	 * vertex in the edge-weighted digraph {@code G}.
	 *
	 * @param  G the edge-weighted digraph
	 * @param  s the source vertex
	 * @throws IllegalArgumentException if an edge weight is negative
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public DijkstraSP(Graph G, String s) {
		
		numPathTo = s;

		Iterator<Arco> it = G.getArcos().iterator();
		while(it.hasNext()) {

			Arco e = it.next();

			if ((Integer) e.getInfo() < 0) {
				throw new IllegalArgumentException("edge " + e + " has negative weight");
			}

		}

		//Cargar hashTable con un indice asociado al id del vertice
		lpV = new hashTableLinearProbing<Integer, String>();
		lpV2 = new hashTableLinearProbing<String, Integer>();

		Iterator<String> itIdV = G.idVertices().iterator();
		int contV = 0;
		while(itIdV.hasNext()) {

			String idActual = itIdV.next();
			lpV.put(contV, idActual);
			lpV2.put(idActual, contV);

			contV++;

		}





		distTo = new double[G.getNumVertices()];
		edgeTo = new Arco[G.getNumVertices()];

		validateVertex(s);

		for (int v = 0; v < G.getNumVertices(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		
		int p = lpV2.get(s);
		distTo[p] = 0.0;

		// relax vertices in order of distance from s
		pq = new IndexMinPQ<Double>(G.getNumVertices());
		pq.insert(p, distTo[p]);
		while (!pq.isEmpty()) {
			int v = pq.delMin();
			//for (DirectedEdge e : G.adj(v))

			String idVActual = lpV.get(v);
			Vertice ve = (Vertice) G.getVertices().get(idVActual);


			Iterator<Arco> arcosV = ve.getArcos().iterator();
			while(arcosV.hasNext()) {

				Arco e = arcosV.next();

				relax(e);


			}


		}

		// check optimality conditions
		assert check(G, p);
	}

	// relax edge e and update pq if changed
	private void relax(Arco e) {

		String vIni = (String) e.getV1();
		String vFin = (String) e.getV2();


		int posI = lpV2.get(vIni);
		int posF = lpV2.get(vFin);

		

		int v = posI, w = posF;
		if (distTo[w] > distTo[v] + (Integer)e.getInfo()) {
			distTo[w] = distTo[v] + (Integer)e.getInfo();
			edgeTo[w] = e;
			if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
			else                pq.insert(w, distTo[w]);
		}
	}

	/**
	 * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
	 * @param  v the destination vertex
	 * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
	 *         {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public double distTo(int v) {
		
		String parse = Integer.toString(v);
		validateVertex(parse);
		return distTo[v];
	}

	/**
	 * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return {@code true} if there is a path from the source vertex
	 *         {@code s} to vertex {@code v}; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(String v) {
		//String parse = Integer.toString(v);
		validateVertex(v);
		
		int pos = lpV2.get(v);
		
		return distTo[pos] < Double.POSITIVE_INFINITY;
	}
	/**
	 * Returns a shortest path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return a shortest path from the source vertex {@code s} to vertex {@code v}
	 *         as an iterable of edges, and {@code null} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<Arco> pathTo(String v) {

		validateVertex(v);
		if (!hasPathTo(v)) return null;
		Stack<Arco> path = new Stack<Arco>();

		
		int pos = lpV2.get(v);
		
		int pos2 = lpV2.get(numPathTo);
		
		Arco e2 = edgeTo[pos2];
		
		//int cont = 0;
		
		for (Arco e = edgeTo[pos]; e != null && e.equals(e2) == false; e = edgeTo[lpV2.get((String)e.getV1())]) {
			//cont++;
			path.push(e);

		}
		
		//System.out.println("CONT DJ " +cont);
		
		
//		for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
//            path.push(e);
//        }
		
		return path;
	}


	// check optimality conditions:
	// (i) for all edges e:            distTo[e.to()] <= distTo[e.from()] + e.weight()
	// (ii) for all edge e on the SPT: distTo[e.to()] == distTo[e.from()] + e.weight()
	private boolean check(Graph G, int s) {

		// check that edge weights are nonnegative

		Iterator<Arco> itArcos = G.getArcos().iterator();

		while(itArcos.hasNext()) {

			Arco e = itArcos.next();

			if ((Double) e.getInfo() < 0) {
				System.err.println("negative edge weight detected");
				return false;
			}


		}

	

	// check that distTo[v] and edgeTo[v] are consistent
	if (distTo[s] != 0.0 || edgeTo[s] != null) {
		System.err.println("distTo[s] and edgeTo[s] inconsistent");
		return false;
	}
	for (int v = 0; v < G.getNumVertices(); v++) {
		if (v == s) continue;
		if (edgeTo[v] == null && distTo[v] != Double.POSITIVE_INFINITY) {
			System.err.println("distTo[] and edgeTo[] inconsistent");
			return false;
		}
	}

	// check that all edges e = v->w satisfy distTo[w] <= distTo[v] + e.weight()
	for (int v = 0; v < G.getNumVertices(); v++) {
		
		String id = lpV.get(v);
		Vertice ve = (Vertice) G.getVertices().get(id);
		
		Iterator<Arco> arcos = ve.getArcos().iterator();
		
		while(arcos.hasNext()) {
			
			Arco e = arcos.next();
			String idV1 = (String) e.getV1();
			
			int w = lpV2.get(idV1);
			
			if (distTo[v] + (Double) e.getInfo()< distTo[w]) {
				System.err.println("edge " + e + " not relaxed");
				return false;
			}
			
			
		}
		
		
//		for (DirectedEdge e : G.adj(v)) {
//			int w = e.to();
//			if (distTo[v] + e.weight() < distTo[w]) {
//				System.err.println("edge " + e + " not relaxed");
//				return false;
//			}
//		}
	}

	// check that all edges e = v->w on SPT satisfy distTo[w] == distTo[v] + e.weight()
//	for (int w = 0; w < G.V(); w++) {
//		if (edgeTo[w] == null) continue;
//		Arco e = edgeTo[w];
//		int v = e.from();
//		if (w != e.to()) return false;                   TERMINAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARRRRRRRRR
//		if (distTo[v] + e.weight() != distTo[w]) {
//			System.err.println("edge " + e + " on shortest path not tight");
//			return false;
//		}
//	}
	return true;
}

// throw an IllegalArgumentException unless {@code 0 <= v < V}
private void validateVertex(String v) {
	int V = distTo.length;
	
	//long v2 = Long.parseLong(v);
	
	int pos = lpV2.get(v);
	
	
	
	if (pos < 0 || pos >= V)
		throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
}

//	/**
//	 * Unit tests the {@code DijkstraSP} data type.
//	 *
//	 * @param args the command-line arguments
//	 */
//	public static void main(String[] args) {
//		In in = new In(args[0]);
//		EdgeWeightedDigraph G = new EdgeWeightedDigraph(in);
//		int s = Integer.parseInt(args[1]);
//
//		// compute shortest paths
//		DijkstraSP sp = new DijkstraSP(G, s);
//
//
//		// print shortest path
//		for (int t = 0; t < G.V(); t++) {
//			if (sp.hasPathTo(t)) {
//				StdOut.printf("%d to %d (%.2f)  ", s, t, sp.distTo(t));
//				for (DirectedEdge e : sp.pathTo(t)) {
//					StdOut.print(e + "   ");
//				}
//				StdOut.println();
//			}
//			else {
//				StdOut.printf("%d to %d         no path\n", s, t);
//			}
//		}
//	}

}