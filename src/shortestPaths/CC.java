package shortestPaths;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.data_structures.Bag;
import model.data_structures.Graph;
import model.data_structures.Vertice;
import model.data_structures.hashTableSeparateChaining;


/**************************
 *  Compilation:  javac CC.java
 *  Execution:    java CC filename.txt
 *  Dependencies: Graph.java StdOut.java Queue.java
 *  Data files:   https://algs4.cs.princeton.edu/41graph/tinyG.txt
 *                https://algs4.cs.princeton.edu/41graph/mediumG.txt
 *                https://algs4.cs.princeton.edu/41graph/largeG.txt
 *
 *  Compute connected components using depth first search.
 *  Runs in O(E + V) time.
 *
 *  % java CC tinyG.txt
 *  3 components
 *  0 1 2 3 4 5 6
 *  7 8 
 *  9 10 11 12
 *
 *  % java CC mediumG.txt 
 *  1 components
 *  0 1 2 3 4 5 6 7 8 9 10 ...
 *
 *  % java -Xss50m CC largeG.txt 
 *  1 components
 *  0 1 2 3 4 5 6 7 8 9 10 ...
 *
 *  Note: This implementation uses a recursive DFS. To avoid needing
 *        a potentially very large stack size, replace with a non-recurisve
 *        DFS ala NonrecursiveDFS.java.
 *
 **************************/

/**
 *  The {@code CC} class represents a data type for 
 *  determining the connected components in an undirected graph.
 *  The <em>id</em> operation determines in which connected component
 *  a given vertex lies; the <em>connected</em> operation
 *  determines whether two vertices are in the same connected component;
 *  the <em>count</em> operation determines the number of connected
 *  components; and the <em>size</em> operation determines the number
 *  of vertices in the connect component containing a given vertex.

 *  The <em>component identifier</em> of a connected component is one of the
 *  vertices in the connected component: two vertices have the same component
 *  identifier if and only if they are in the same connected component.

 *  <p>
 *  This implementation uses depth-first search.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>
 *  (in the worst case),
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Afterwards, the <em>id</em>, <em>count</em>, <em>connected</em>,
 *  and <em>size</em> operations take constant time.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>   
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class CC {
	private hashTableSeparateChaining<String, Boolean> marked;   // marked[v] = has vertex v been marked?
	private hashTableSeparateChaining<String, Integer>  id;      // id[v] = id of connected component containing v
	private hashTableSeparateChaining<Integer, Integer>    size;     // size[id] = number of vertices in given component
	private int count;          // number of connected components

	/**
	 * Computes the connected components of the edge-weighted graph {@code G}.
	 *
	 * @param G the edge-weighted graph
	 */
	public CC(Graph G) {
		marked = new hashTableSeparateChaining<>();
		count = 0;
		
		Iterator<String> it = G.getVertices().keys().iterator();
		while (it.hasNext())
		{
			String key = it.next();
			marked.put(key, false);
		}
		
		id = new hashTableSeparateChaining<>();
		size = new hashTableSeparateChaining<>();
		it = G.getVertices().keys().iterator();
		for (int i = 0; i < G.getNumVertices(); i++) {
			String v = it.next();
			if (!marked.get(v)) {
				dfs(G, v);
				count++;
			}
		}
	}

	
	Bag<Vertice> hola = new Bag<Vertice>();
	
	
	
	
	
	public Bag<Vertice> getVertices() {
		
		return hola;
		
		
	}
	
	// depth-first search for an EdgeWeightedGraph
	private void dfs(Graph G, String v) {
		marked.put(v, true);
		id.put(v, count);
		if (size.get(count) == null)
			size.put(count, 1);
		else
			size.put( count, (size.get(count)+1) );
		
		
		Vertice ve = (Vertice)G.getVertices().get(v);
		
		hola.add(ve);
		
		Iterator<String> it = ve.getAdj().iterator();
		
		while(it.hasNext()) {
			
			ve = (Vertice) G.getVertices().get(it.next());
			
			if (!marked.get((String)ve.getId())) {
				dfs(G, (String) ve.getId());
			}
			
			
		}
		
		
		
//		for (Vertice w : (List<Vertice>) G.getVertice(v).getAdj() ) {
//			if (!marked.get(w.getKey())) {
//				dfs(G, w.getKey());
//			}
//		}
	}


	/**
	 * Returns the component id of the connected component containing vertex {@code v}.
	 *
	 * @param  v the vertex
	 * @return the component id of the connected component containing vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int id(String v) {
		return id.get(v);
	}

	/**
	 * Returns the number of vertices in the connected component containing vertex {@code v}.
	 *
	 * @param  v the vertex
	 * @return the number of vertices in the connected component containing vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int size(String v) {
		return size.get(id.get(v));
	}

	/**
	 * Returns the number of connected components in the graph {@code G}.
	 *
	 * @return the number of connected components in the graph {@code G}
	 */
	public int count() {
		return count;
	}

	/**
	 * Returns true if vertices {@code v} and {@code w} are in the same
	 * connected component.
	 *
	 * @param  v one vertex
	 * @param  w the other vertex
	 * @return {@code true} if vertices {@code v} and {@code w} are in the same
	 *         connected component; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 * @throws IllegalArgumentException unless {@code 0 <= w < V}
	 */
	public boolean connected(String v, String w) {
		return id.get(v) == id.get(w);
	}

}