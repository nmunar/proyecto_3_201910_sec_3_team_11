package shortestPaths;

import java.util.Iterator;

import model.data_structures.Graph;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.hashTableLinearProbing;
/******************************************************************************
 *  Compilation:  javac BreadthFirstPaths.java
 *  Execution:    java BreadthFirstPaths G s
 *  Dependencies: Graph.java Queue.java Stack.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/41graph/tinyCG.txt
 *                https://algs4.cs.princeton.edu/41graph/tinyG.txt
 *                https://algs4.cs.princeton.edu/41graph/mediumG.txt
 *                https://algs4.cs.princeton.edu/41graph/largeG.txt
 *
 *  Run breadth first search on an undirected graph.
 *  Runs in O(E + V) time.
 *
 *  %  java Graph tinyCG.txt
 *  6 8
 *  0: 2 1 5 
 *  1: 0 2 
 *  2: 0 1 3 4 
 *  3: 5 4 2 
 *  4: 3 2 
 *  5: 3 0 
 *
 *  %  java BreadthFirstPaths tinyCG.txt 0
 *  0 to 0 (0):  0
 *  0 to 1 (1):  0-1
 *  0 to 2 (1):  0-2
 *  0 to 3 (2):  0-2-3
 *  0 to 4 (2):  0-2-4
 *  0 to 5 (1):  0-5
 *
 *  %  java BreadthFirstPaths largeG.txt 0
 *  0 to 0 (0):  0
 *  0 to 1 (418):  0-932942-474885-82707-879889-971961-...
 *  0 to 2 (323):  0-460790-53370-594358-780059-287921-...
 *  0 to 3 (168):  0-713461-75230-953125-568284-350405-...
 *  0 to 4 (144):  0-460790-53370-310931-440226-380102-...
 *  0 to 5 (566):  0-932942-474885-82707-879889-971961-...
 *  0 to 6 (349):  0-932942-474885-82707-879889-971961-...
 *
 ******************************************************************************/


/**
 *  The {@code BreadthFirstPaths} class represents a data type for finding
 *  shortest paths (number of edges) from a source vertex <em>s</em>
 *  (or a set of source vertices)
 *  to every other vertex in an undirected graph.
 *  <p>
 *  This implementation uses breadth-first search.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Each call to {@link #distTo(int)} and {@link #hasPathTo(int)} takes constant time;
 *  each call to {@link #pathTo(int)} takes time proportional to the length
 *  of the path.
 *  It uses extra space (not including the graph) proportional to <em>V</em>.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>   
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class BFS {
	private static final int INFINITY = Integer.MAX_VALUE;
	private boolean[] marked;  // marked[v] = is there an s-v path
	private String[] edgeTo;      // edgeTo[v] = previous edge on shortest s-v path
	private int[] distTo;      // distTo[v] = number of edges shortest s-v path

	private hashTableLinearProbing<Integer, String> lista1 = new hashTableLinearProbing<>();
	private hashTableLinearProbing<String, Integer> lista2 = new hashTableLinearProbing<>();

	/**
	 * Computes the shortest path between the source vertex {@code s}
	 * and every other vertex in the graph {@code G}.
	 * @param G the graph
	 * @param s the source vertex
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public BFS(Graph<String, String, Double> G, String s) {
		marked = new boolean[G.getNumVertices()];
		distTo = new int[G.getNumVertices()];
		edgeTo = new String[G.getNumVertices()];
		Iterator<String> it = G.idVertices().iterator();
		int cont = 0;
		while(it.hasNext())
		{
			String id = (String) G.getVertices().get(it.next()).getId();
			lista1.put(cont, id);
			lista2.put(id, cont);
			cont++;
		}

		validateVertex(s);
		bfs(G, s);

		assert check(G, s);
	}

	/**
	 * Computes the shortest path between any one of the source vertices in {@code sources}
	 * and every other vertex in graph {@code G}.
	 * @param G the graph
	 * @param sources the source vertices
	 * @throws IllegalArgumentException unless {@code 0 <= s < V} for each vertex
	 *         {@code s} in {@code sources}
	 */
	//    public BFS(Graph<String, String, Double> G, Iterable<Integer> sources) {
	//        marked = new boolean[G.getNumVertices()];
	//        distTo = new int[G.getNumVertices()];
	//        edgeTo = new String[G.getNumVertices()];
	//        for (int v = 0; v < G.getNumVertices(); v++)
	//            distTo[v] = INFINITY;
	//        validateVertices(sources);
	//        bfs(G, sources);
	//    }


	// breadth-first search from a single source
	private void bfs(Graph<String, String, Double> G, String s) {
		Queue<String> q = new Queue<String>();
		for (int v = 0; v < G.getNumVertices(); v++)
			distTo[v] = INFINITY;

		int pos = lista2.get(s);
		distTo[pos] = 0;
		marked[pos] = true;
		q.enqueue(s);

		while (!q.isEmpty()) {
			String v = q.dequeue();
			Iterator<String> it = G.adj(v);	
			while(it.hasNext())
			{
				String key = it.next();
				int w =lista2.get(key);
				int v1 = lista2.get(v);
				if (!marked[w]) {
					edgeTo[w] = v;
					distTo[w] = distTo[v1] + 1;
					marked[w] = true;
					q.enqueue(key);
				}
			}
		}
	}

	//    // breadth-first search from multiple sources
	//    private void bfs(Graph<String, String, Double> G, Iterable<String> sources) {
	//        Queue<String> q = new Queue<String>();
	//        for (String s : sources) {
	//        	int pos = lista2.get(s);
	//            marked[pos] = true;
	//            distTo[pos] = 0;
	//            q.enqueue(s);
	//        }
	//        while (!q.isEmpty()) {
	//        	 String v = q.dequeue();
	//             Iterator<String> it = G.adj(v);	
	// 			while(it.hasNext())
	// 			{
	// 				String key = it.next();
	// 				int w =lista2.get(key);
	//				int v1 = lista2.get(v);
	//                if (!marked[w]) {
	//                    edgeTo[w] = v;
	//                    distTo[w] = distTo[v1] + 1;
	//                    marked[w] = true;
	//                    q.enqueue(key);
	//                }
	//            }
	//        }
	//    }

	/**
	 * Is there a path between the source vertex {@code s} (or sources) and vertex {@code v}?
	 * @param v the vertex
	 * @return {@code true} if there is a path, and {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(String v) {
		int pos = lista2.get(v);
		validateVertex(v);
		return marked[pos];
	}

	/**
	 * Returns the number of edges in a shortest path between the source vertex {@code s}
	 * (or sources) and vertex {@code v}?
	 * @param v the vertex
	 * @return the number of edges in a shortest path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int distTo(String v) {
		int pos = lista2.get(v);
		validateVertex(v);
		return distTo[pos];
	}

	/**
	 * Returns a shortest path between the source vertex {@code s} (or sources)
	 * and {@code v}, or {@code null} if no such path.
	 * @param  v the vertex
	 * @return the sequence of vertices on a shortest path, as an Iterable
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<String> pathTo(String v) {

		validateVertex(v);

		if (!hasPathTo(v)) return null;
		Stack<String> path = new Stack<String>();
		String x;
		for (x = v; distTo[lista2.get(x)] != 0; x = edgeTo[lista2.get(x)])
			path.push(x);
		path.push(x);
		return path;
	}


	// check optimality conditions for single source
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private boolean check(Graph G, String s) {

		// check that the distance of s = 0
		int pos = lista2.get(s);
		if (distTo[pos] != 0) {
			System.out.println("distance of source " + s + " to itself = " + distTo[pos]);
			return false;
		}

		// check that for each edge v-w dist[w] <= dist[v] + 1
		// provided v is reachable from s
		for (int v = 0; v < G.getNumVertices(); v++) {
			Iterator<String> it = G.adj(v);
			String v1 = lista1.get(v);
			while(it.hasNext())
			{
				String w = it.next();
				int pos1 = lista2.get(w);

				if (hasPathTo(v1) != hasPathTo(w)) {
					System.out.println("edge " + v + "-" + w);
					System.out.println("hasPathTo(" + v + ") = " + hasPathTo(v1));
					System.out.println("hasPathTo(" + w + ") = " + hasPathTo(w));
					return false;
				}
				if (hasPathTo(v1) && (distTo[pos1] > distTo[v] + 1)) {
					System.out.println("edge " + v + "-" + w);
					System.out.println("distTo[" + v + "] = " + distTo[v]);
					System.out.println("distTo[" + w + "] = " + distTo[pos1]);
					return false;
				}
			}
		}

		// check that v = edgeTo[w] satisfies distTo[w] = distTo[v] + 1
		// provided v is reachable from s
		for (int w = 0; w < G.getNumVertices(); w++) {
			String w1 = lista1.get(w);
			if (!hasPathTo(w1) || w1 == s) continue;
			String v = edgeTo[w];
			int v1 = lista2.get(v);
			if (distTo[w] != distTo[v1] + 1) {
				System.out.println("shortest path edge " + v + "-" + w);
				System.out.println("distTo[" + v + "] = " + distTo[v1]);
				System.out.println("distTo[" + w + "] = " + distTo[w]);
				return false;
			}
		}

		return true;
	}

	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	private void validateVertex(String v) {
		int V = marked.length;
		int pos = lista2.get(v);
		if (pos < 0 || pos >= V)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	}

	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	@SuppressWarnings("unused")
	private void validateVertices(Iterable<Integer> vertices) {
		if (vertices == null) {
			throw new IllegalArgumentException("argument is null");
		}
		int V = marked.length;
		for (int v : vertices) {
			if (v < 0 || v >= V) {
				throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
			}
		}
	}
}
