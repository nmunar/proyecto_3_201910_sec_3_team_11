package shortestPaths;

import java.util.Iterator;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.IndexMinPQ;
import model.data_structures.Queue;
import model.data_structures.Vertice;
import model.data_structures.hashTableLinearProbing;

/**
 *  The {@code PrimMST} class represents a data type for computing a
 *  <em>minimum spanning tree</em> in an edge-weighted graph.
 *  The edge weights can be positive, zero, or negative and need not
 *  be distinct. If the graph is not connected, it computes a <em>minimum
 *  spanning forest</em>, which is the union of minimum spanning trees
 *  in each connected component. The {@code weight()} method returns the 
 *  weight of a minimum spanning tree and the {@code edges()} method
 *  returns its edges.
 *  <p>
 *  This implementation uses <em>Prim's algorithm</em> with an indexed
 *  binary heap.
 *  The constructor takes time proportional to <em>E</em> log <em>V</em>
 *  and extra space (not including the graph) proportional to <em>V</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Afterwards, the {@code weight()} method takes constant time
 *  and the {@code edges()} method takes time proportional to <em>V</em>.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/43mst">Section 4.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  For alternate implementations, see {@link LazyPrimMST}, {@link KruskalMST},
 *  and {@link BoruvkaMST}.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class PrimMST {
	private static final double FLOATING_POINT_EPSILON = 1E-12;

	private Arco[] edgeTo;        // edgeTo[v] = shortest edge from tree vertex to non-tree vertex
	private double[] distTo;      // distTo[v] = weight of shortest such edge
	private boolean[] marked;     // marked[v] = true if v on tree, false otherwise
	private IndexMinPQ<Double> pq;

	private hashTableLinearProbing<Integer, String> lpV;
	private hashTableLinearProbing<String, Integer> lpV2;
	private hashTableLinearProbing<Integer, Arco> lpV3;
	private hashTableLinearProbing<Arco,Integer > lpV4;

	/**
	 * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
	 * @param G the edge-weighted graph
	 */
	public PrimMST(Graph G) {
		edgeTo = new Arco[G.getNumVertices()];
		distTo = new double[G.getNumVertices()];
		marked = new boolean[G.getNumVertices()];
		pq = new IndexMinPQ<Double>(G.getNumVertices());
		for (int v = 0; v < G.getNumVertices(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;

		lpV = new hashTableLinearProbing<Integer, String>();
		lpV2 = new hashTableLinearProbing<String, Integer>();
		lpV3 = new hashTableLinearProbing<Integer, Arco>();
		lpV4 = new hashTableLinearProbing<Arco, Integer>();

		Iterator<String> itIdV = G.idVertices().iterator();
		int contV = 0;
		while(itIdV.hasNext()) {

			String idActual = itIdV.next();
			lpV.put(contV, idActual);
			lpV2.put(idActual, contV);

			contV++;

		}

		Iterator<Arco> itArcos = G.getArcos().iterator();
		int contA = 0;
		while(itArcos.hasNext()) {

			Arco a = itArcos.next();
			lpV3.put(contA, a);
			lpV4.put(a, contA);


			contA++;
		}

		for (int v = 0; v < G.getNumVertices(); v++)    {
			// run from each vertex to find
			String t = lpV.get(v);
			if (!marked[v]) prim(G, t);      // minimum spanning forest
		}



		// check optimality conditions
		// assert check(G);
	}

	// run Prim's algorithm in graph G, starting from vertex s
	private void prim(Graph G, String s) {

		int pos = lpV2.get(s);

		distTo[pos] = 0.0;
		pq.insert(pos, distTo[pos]);
		while (!pq.isEmpty()) {
			int v = pq.delMin();

			String t = lpV.get(v);

			scan(G, t);
		}
	}

	// scan vertex v
	private void scan(Graph G, String v) {
		int pos = lpV2.get(v);

		marked[pos] = true;

		Iterator<String> vertices = G.getVertices().keys().iterator();
		while(vertices.hasNext()) {

			String veId = vertices.next();
			Vertice ve = (Vertice) G.getVertices().get(veId);

			Iterator<Arco> arcos = ve.getArcos().iterator();

			while(arcos.hasNext()) {

				Arco a = arcos.next();

				//int p = lpV2.get(v);

				int y = lpV2.get(v);

				if(a.getV1().equals(v)) {

					String x = (String) a.getV2();
					y = lpV2.get(x);

				} else if(a.getV2().equals(v)){

					String x = (String) a.getV1();
					y = lpV2.get(x);

				}

				int w = y;

				if (marked[w]) continue;         // v-w is obsolete edge
				if ((Double) a.getInfo() < distTo[w]) {
					distTo[w] = (Double) a.getInfo();
					edgeTo[w] = a;
					if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
					else                pq.insert(w, distTo[w]);
				}

			}



		}


	}

	/**
	 * Returns the edges in a minimum spanning tree (or forest).
	 * @return the edges in a minimum spanning tree (or forest) as
	 *    an iterable of edges
	 */
	public Iterable<Arco> edges() {
		Queue<Arco> mst = new Queue<Arco>();
		for (int v = 0; v < edgeTo.length; v++) {
			Arco e = edgeTo[v];
			if (e != null) {
				mst.enqueue(e);
			}
		}
		return mst;
	}

	/**
	 * Returns the sum of the edge weights in a minimum spanning tree (or forest).
	 * @return the sum of the edge weights in a minimum spanning tree (or forest)
	 */
	public double weight() {
		double weight = 0.0;
		for (Arco e : edges())
			weight += (Double) e.getInfo();
		return weight;
	}


	//    // check optimality conditions (takes time proportional to E V lg* V)
	//    private boolean check(EdgeWeightedGraph G) {
	//
	//        // check weight
	//        double totalWeight = 0.0;
	//        for (Edge e : edges()) {
	//            totalWeight += e.weight();
	//        }
	//        if (Math.abs(totalWeight - weight()) > FLOATING_POINT_EPSILON) {
	//            System.err.printf("Weight of edges does not equal weight(): %f vs. %f\n", totalWeight, weight());
	//            return false;
	//        }
	//
	//        // check that it is acyclic
	//        UF uf = new UF(G.V());
	//        for (Edge e : edges()) {
	//            int v = e.either(), w = e.other(v);
	//            if (uf.connected(v, w)) {
	//                System.err.println("Not a forest");
	//                return false;
	//            }
	//            uf.union(v, w);
	//        }
	//
	//        // check that it is a spanning forest
	//        for (Edge e : G.edges()) {
	//            int v = e.either(), w = e.other(v);
	//            if (!uf.connected(v, w)) {
	//                System.err.println("Not a spanning forest");
	//                return false;
	//            }
	//        }
	//
	//        // check that it is a minimal spanning forest (cut optimality conditions)
	//        for (Edge e : edges()) {
	//
	//            // all edges in MST except e
	//            uf = new UF(G.V());
	//            for (Edge f : edges()) {
	//                int x = f.either(), y = f.other(x);
	//                if (f != e) uf.union(x, y);
	//            }
	//
	//            // check that e is min weight edge in crossing cut
	//            for (Edge f : G.edges()) {
	//                int x = f.either(), y = f.other(x);
	//                if (!uf.connected(x, y)) {
	//                    if (f.weight() < e.weight()) {
	//                        System.err.println("Edge " + f + " violates cut optimality conditions");
	//                        return false;
	//                    }
	//                }
	//            }
	//
	//        }
	//
	//        return true;
	//    }
	//
	//    /**
	//     * Unit tests the {@code PrimMST} data type.
	//     *
	//     * @param args the command-line arguments
	//     */
	//    public static void main(String[] args) {
	//        In in = new In(args[0]);
	//        EdgeWeightedGraph G = new EdgeWeightedGraph(in);
	//        PrimMST mst = new PrimMST(G);
	//        for (Edge e : mst.edges()) {
	//            StdOut.println(e);
	//        }
	//        StdOut.printf("%.5f\n", mst.weight());
	//    }


}