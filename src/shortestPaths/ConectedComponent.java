package shortestPaths;

import model.data_structures.Arco;
import model.data_structures.Bag;
import model.data_structures.Vertice;

public class ConectedComponent {
	
	private int cantidadVertices;
	
	private Bag<Vertice> vertices;
	
	private Bag<Arco> arcos;
	
	
	public ConectedComponent(int pCV, Bag<Vertice> pVertices, Bag<Arco> pArcos) {
		
		cantidadVertices = pCV;
		vertices = pVertices;
		arcos = pArcos;
		
		
	}
	
	
	
	public int getCantidadVertices() {
		
		return cantidadVertices;
		
	}
	
	
	public Bag<Vertice> getVertices() {
		
		return vertices;
		
	}
	
	public Bag<Arco> getArcos() {
		
		return arcos;
		
	}
	
	

}
