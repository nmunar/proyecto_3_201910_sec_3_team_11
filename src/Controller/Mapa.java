package Controller;

import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.JFrame;

import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.Vertice;

import com.teamdev.jxmaps.*;
public class Mapa extends MapView
{
	private Map map;
	private Graph grafo;
	public Mapa(String pName, Graph<String, String, Double> pGrafo, int p)
	{
		grafo = pGrafo;
		JFrame frame = new JFrame(pName);

		setOnMapReadyHandler(new MapReadyHandler() {


			@Override
			public void onMapReady(MapStatus status) {

				if(status== MapStatus.MAP_STATUS_OK)
				{
					map = getMap();

					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					mapOptions.setMapTypeControlOptions(controlOptions);
					
					map.setOptions(mapOptions);					
					map.setCenter(new LatLng(38.899035, -77.025995));
					map.setZoom(13.8);				

					//					Marker mark =  new Marker(map);
					//					mark.setPosition(map.getCenter());
					Iterator<String> iter = grafo.idVertices().iterator();
					while(iter.hasNext())
					{
						String key = iter.next();
						Vertice vert = (Vertice) grafo.getVertices().get(key);

						if(vert.hayArcos()==true && p==1)
						{
							
							Iterator<Arco> arc = vert.getArcos().iterator();
							
							
							while(arc.hasNext())
							{
																
								Arco arco = arc.next();
								Vertice v1 = (Vertice) grafo.getVertices().get(arco.getV1());
								Vertice v2 = (Vertice) grafo.getVertices().get(arco.getV2());
								Double lat1 = Double.parseDouble((String)v1.getLatitud());
								Double lon1 = Double.parseDouble((String)v1.getLongitud());

								Double lat2 = Double.parseDouble((String)v2.getLatitud());
								Double lon2 = Double.parseDouble((String)v2.getLongitud());

								//-------------------------------------------------------------------------------------------

								Polyline line = new Polyline(map);

								LatLng[] path3 = {new LatLng(map, lat1, lon1),
										new LatLng(map, lat2, lon2)};

								line.setPath(path3);
								line.setVisible(true);
								PolylineOptions li = new PolylineOptions();
								li.setStrokeColor("#FFFFFF");
								li.setStrokeOpacity(1);
								li.setStrokeWeight(3);
								line.setOptions(li);

								//-------------------------------------------------------------------------------------------
								LatLng path = new LatLng(lat1,lon1);
								Circle circle = new Circle(map);
								circle.setCenter(path);
								circle.setRadius(5);

								CircleOptions co = new CircleOptions();
								co.setFillColor("#FF0000");
								co.setFillOpacity(3);
								co.setStrokeColor("#FF0000");

								circle.setOptions(co);
								//-------------------------------------------------------------------------------------------							
								LatLng path2 = new LatLng(lat2,lon2);
								Circle circle2 = new Circle(map);
								circle2.setCenter(path2);
								circle2.setRadius(3);

								CircleOptions co2 = new CircleOptions();
								co2.setFillColor("#FF0000");
								co2.setFillOpacity(1);
								co2.setStrokeColor("#FF0000");

								circle2.setOptions(co2);

							}


						}else if(p==2)
						{
							
							
							LatLng path = new LatLng(Double.parseDouble((String) vert.getLatitud()),Double.parseDouble((String)vert.getLongitud()));
							Circle circle = new Circle(map);
							circle.setCenter(path);
							circle.setRadius(5);

							CircleOptions co = new CircleOptions();
							co.setFillColor("#FF0000");
							co.setFillOpacity(3);
							co.setStrokeColor("#FF0000");

							circle.setOptions(co);
						}
					}

				}

			}
		});

		frame.add(this,BorderLayout.CENTER);
		frame.setSize(700,500);
		frame.setVisible(true);
	}

	//	public static void main(String[] arg)
	//	{
	//		Mapa tamp = new Mapa("Grafo Cargado",grafo);
	//	}
}
