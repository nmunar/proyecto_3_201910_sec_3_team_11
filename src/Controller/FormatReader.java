package Controller;

import java.util.ArrayList;
import java.util.Iterator;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//import jdk.nashorn.internal.parser.JSONParser;
import model.data_structures.Bag;
import model.data_structures.Graph;
import model.data_structures.Vertice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;




public class FormatReader extends DefaultHandler{

	private JSONObject vertexList = new JSONObject();
	private int contVertex = 0;
	private JSONArray vertexArray = new JSONArray();
	private JSONArray edgesArray = new JSONArray();

	private Graph<String, String, Double> grafo = new Graph<String, String, Double>();
	private Bag<String> arcos = new Bag<String>();

	private StringBuilder buffer = new StringBuilder();
	private Bag<String> idVertices= new Bag<String>();

	private String id;
	private String lon;
	private String lat;
	private boolean isHighway =false;
	private boolean finNode=false;
	private boolean finWays = false;


	private double maxLat = 0;
	private double minLat = 0;
	private double maxLon = 0;
	private double minLon = 0;

	private int contP = 0;


	//Carga de datos

	private Bag<Vertice> vertices = new Bag<Vertice>();


	public Graph<String,String,Double> getGrafo(){
		return grafo;
	}

	//	public void characters(char[] ch, int start, int length) throws SAXException {
	//		buffer.append(ch,start,length);
	//	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch(qName)
		{
		case"node":
			isHighway=false;
			break;
		case "way":
			if(isHighway==true)
			{
				Iterator<String> iter = arcos.iterator();
				int cont =0;

				String key = (String) iter.next();
				String key2 =null;


				while(iter.hasNext()) 
				{

					if(cont!=0) {
						key = key2;
					}
					key2 = (String)iter.next(); 

					Double lat1 = Double.parseDouble(grafo.getVertices().get(key).getLatitud().toString());
					Double lon1 = Double.parseDouble(grafo.getVertices().get(key).getLongitud().toString());
					Double lat2 = Double.parseDouble(grafo.getVertices().get(key2).getLatitud().toString());
					Double lon2 = Double.parseDouble(grafo.getVertices().get(key2).getLongitud().toString());

					grafo.addEdge(key, key2, distance(lat1,lon1,lat2,lon2));
					
					grafo.getVertices().get(key).addAdj(key2);
					grafo.getVertices().get(key2).addAdj(key);
					
					Double distance = distance(lat1, lon1, lat2, lon2);
					JSONObject edge = new JSONObject();
					edge.put("K1", key);
					edge.put("K2", key2);
					edge.put("D", distance);
					edgesArray.add(edge);
					cont=1;

				}

				arcos = new Bag<>();
				id    = null;
				isHighway = false;
			}else
			{
				arcos = new Bag<>();
				id    = null;

			}

			break;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch(qName)
		{
		case "bounds":
			System.out.print("MaxLon = ");
			System.out.println(attributes.getValue("maxlon"));
			System.out.print("MaxLat = ");
			System.out.println(attributes.getValue("maxlat"));
			System.out.print("MinLon = ");
			System.out.println(attributes.getValue("minlon"));
			System.out.print("MinLat = ");
			System.out.println(attributes.getValue("minlat"));

			maxLat = Double.parseDouble(attributes.getValue("maxlat"));
			minLat = Double.parseDouble(attributes.getValue("minlat"));
			maxLon = Double.parseDouble(attributes.getValue("maxlon"));
			minLon = Double.parseDouble(attributes.getValue("minlon"));

			break;


		case "node":

			//			contP++;

			lon = attributes.getValue("lon");
			lat = attributes.getValue("lat");
			id = attributes.getValue("id");
			String info = lon+"|"+lat;


			//Se agrega un nuevo v�rtice al bag de v�rtices 
			Vertice nuevoV= new Vertice(id, info);
			nuevoV.setLat(lat);
			nuevoV.setLon(lon);
			vertices.add(nuevoV);

			grafo.addVertex(id, info);

			grafo.getVertices().get(id).setLat(lat);
			grafo.getVertices().get(id).setLon(lon);
			idVertices.add(id);


			JSONObject obj = new JSONObject();
			obj.put("ID", id);
			obj.put("LON", lon);
			obj.put("LAT", lat);
			vertexArray.add(obj);

			break;
		case "way":

			id = attributes.getValue("id");

			break;
		case "nd":
			arcos.add(attributes.getValue("ref"));
			break;

		case "tag":
			if(attributes.getValue("k").equals("highway")) 
			{
				isHighway = true;
			}

			break;
		}


	}


	private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

	public static double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}
	// public Bag<String> darIdVerdices()
	// {
	// return idVertices;
	// }

	public void writeJson(JSONArray pObj, JSONArray pObj2) {
		JSONObject obj = new JSONObject();
		obj.put("vertices", pObj);
		obj.put("arcos", pObj2);
		try {
			String ruta = ".\\data\\persistencia.txt";
			File file = new File(ruta);
			// Si el archivo no existe es creado
			//  if (!file.exists()) {
			file.createNewFile();
			// }
			FileWriter file2 = new FileWriter(file);
			file2.write(obj.toJSONString());
			file2.flush();
			file2.close();

		} catch (IOException e) {
			//manejar error
		}

	}

	public Graph<String,String,Double> readJson() {

		JSONParser parser = new JSONParser();
		grafo = new Graph<String,String,Double>();
		try {
			try {
				JSONObject jsonObject = (JSONObject) parser.parse( new FileReader(".\\data\\persistencia.txt"));
				JSONArray vertices = (JSONArray) jsonObject.get("vertices");
				JSONArray arcos = (JSONArray) jsonObject.get("arcos");

				for(int i = 0; i < vertices.size(); i++) {

					JSONObject vActual = (JSONObject) vertices.get(i);
					String vId = (String) vActual.get("ID");
					String vLon = (String) vActual.get("LON");
					String vLat = (String) vActual.get("LAT");
					String infoC = vLon+"|"+vLat;
					grafo.addVertex(vId, infoC);
					grafo.getVertices().get(vId).setLat(vLat);
					grafo.getVertices().get(vId).setLon(vLon);
				}
				for(int i = 0; i < arcos.size(); i++) {
					JSONObject aActual = (JSONObject) arcos.get(i);
					String k1 = (String) aActual.get("K1");
					String k2 = (String) aActual.get("K2");
					Double infoA = (Double) aActual.get("D");
					grafo.addEdge(k1, k2, infoA);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}  catch (IOException e) {
			e.printStackTrace();
		}
		return grafo;
	}
	public JSONArray getVertexArray() {
		return vertexArray;
	}
	public JSONArray getEdgesArray() {
		return edgesArray;
	}


	public Bag<Vertice> getVerticesBag() {

		return vertices;

	}


	public double getMaxLat() {

		return maxLat;

	}

	public double getMinLat() {

		return minLat;

	}

	public double getMaxLon() {

		return maxLon;

	}

	public double getMinLon() {

		return minLon;

	}

}
