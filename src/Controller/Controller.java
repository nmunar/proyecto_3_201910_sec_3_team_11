package Controller;

import java.awt.List;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import manager.MethodsManager;
import model.data_structures.*;
import shortestPaths.DijkstraSP;

@SuppressWarnings("unused")
public class Controller {

	boolean carga = false;
//Grafos
	private Graph<String, String, Double> grafo;
	private Graph<String, String, Double> grafo2;
	

	MethodsManager metodos; 

	private boolean condic = false;

	private FormatReader handler;
	private JSONArray vertex;
	private JSONArray edge;


	private double maxLat = 0;
	private double minLat = 0;
	private double maxLon = 0;
	private double minLon = 0;



	private double distLon;
	private double distLat;

	Mapa tamp;

	@SuppressWarnings("rawtypes")
	private Bag<Vertice> vertices = new Bag<Vertice>();
	@SuppressWarnings("rawtypes")
	private hashTableLinearProbing<Integer, ArrayList<Vertice>> cuadrantesV = new hashTableLinearProbing<Integer, ArrayList<Vertice>>();

	public Controller() {


	}



	public void menu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 3----------------------");
		System.out.println("1. Cargar el Grafo No Dirigido de la malla vial: Ciudad Completa");

		System.out.println("2. Encontrar el camino de costo mínimo para un viaje entre  dos vertices.. (REQ 1A)");
		System.out.println("3. Determinar los n vértices con mayor número de infracciones y sus componentes conectadas (REQ 2A)");

		System.out.println("4. Encontrar el camino más corto para un viaje entre  dos vertices. (REQ 1B)");		
		System.out.println("5. Definir una cuadricula regular de N columnas por M filas.  (REQ 2B)"); 

		System.out.println("6. Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Kruskal (REQ 1C)");
		System.out.println("7. Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Prim. (REQ 2C)");
		System.out.println("8. Calcular los caminos de costo mínimo con criterio distancia que conecten los vértices resultado "
				+ "de la aproximación de las ubicaciones de la cuadricula N x M encontrados en el punto 5. (REQ 3C)");
		System.out.println("9. Encontrar el camino más corto para un viaje entre dos vertices. (REQ 4C)");

		System.out.println("10. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}


	//Metodo para cargar el grafo de la ciudad de Washington

	private Graph<String, String, Double> newGraph = new Graph<String, String, Double>();

	private Graph<String, String, Integer> grafoD = new Graph<String, String, Integer>();

	@SuppressWarnings("rawtypes")
	private Vertice[] vArray = new Vertice[745747];

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void cargarGrafoJson() {

		int cont = 0;
		carga = true;
		int numInf = 0;

		int pos = 0;

		JsonReader reader;
		JsonParser parser = new JsonParser();



		try {

			String archJson = "./data/finalGraph.json";
			JsonArray jsonArray = (JsonArray) parser.parse(new FileReader(archJson));

			for(int i = 0; i < jsonArray.size();i++) {

				//Atributos del vertice
				String id = "";
				String lon = "";
				String lat = "";
				String info = "";


				JsonObject actual = (JsonObject) jsonArray.get(i);

				id = actual.get("id").getAsString();
				lat = actual.get("lat").getAsString();
				lon = actual.get("lon").getAsString();
				info = lon+"|"+lat;


				Vertice nuevoV= new Vertice(id, info);
				nuevoV.setLat(lat);
				nuevoV.setLon(lon);


				//Se agrega el nuevo vertice al grafo
				newGraph.addVertex(id, info);
				grafoD.addVertex(id, info);

				//Vertice actual
				Vertice v = (Vertice) newGraph.getVertices().get(id);

				//Set lat y lon en el vertice
				newGraph.getVertices().get(id).setLat(lat);
				newGraph.getVertices().get(id).setLon(lon);

				grafoD.getVertices().get(id).setLat(lat);
				grafoD.getVertices().get(id).setLon(lon);


				//Se agrega la cantidad de infracciones asociadas al vertice
				JsonArray infArray = (JsonArray) actual.get("infractions");
				numInf = infArray.size();
				v.setNumInf(numInf);


				//Se agrega a un arreglo que sirve para realizar un sort
				vArray[pos] = v; 


				//Se agrega cada vertice adyacente a este
				JsonArray adjArray = (JsonArray) actual.get("adj");

				for(int j = 0; j < adjArray.size();j++) {

					String idVert = adjArray.get(j).getAsString();	
					v.addAdj(idVert);

				}

				cont += numInf;
				pos++;

			}

			Iterator<String> it =newGraph.idVertices().iterator();
			while(it.hasNext())
			{
				String key = it.next();
				Vertice v = newGraph.getVertices().get(key);

				String lat = (String) v.getLatitud();
				String lon = (String) v.getLongitud();

				Bag<String> listAdjs = v.getAdj();


				Iterator<String> itAdjs = listAdjs.iterator();

				while(itAdjs.hasNext()) {

					String idActual = itAdjs.next();

					Vertice adj = newGraph.getVertices().get(idActual);

					String lonActual = (String) adj.getLongitud();
					String latActual = (String) adj.getLatitud();

					//Calcular distancia harvesiana con el vertice y su adyacente
					Double distancia = distance(Double.parseDouble(lat), Double.parseDouble(lon), Double.parseDouble(latActual), Double.parseDouble(lonActual));



					grafoD.addEdgeDG(key, idActual, numInf);
					newGraph.addEdge(key, idActual, distancia);

				}

			}




		} catch (Exception e) {

		}


		System.out.println("Cantidad de infracciones totales: "+ cont);
		System.out.println("Cantidad de vertices en el grafo: " + newGraph.getNumVertices());
		System.out.println("Cantidad de arcos en el grafo: " + newGraph.getNumArcos());

		System.out.println("Cantidad de vertices en el grafoD: " + grafoD.getNumVertices());
		System.out.println("Cantidad arcos DG: " + grafoD.getNumArcos()+"\n");



		//Iterator<String> ites = grafoD.idVertices().iterator();
		//		
		//		while(ites.hasNext()) {
		//			
		//			String acutal = ites.next();
		//			
		//			Vertice v = (Vertice) grafoD.getVertices().get(acutal);
		//			
		//			System.out.println(v.getLatitud());
		//			System.out.println(v.getLongitud());
		//			
		//			
		//		}

	}


	//Distancia haversiana
	private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

	public static double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}


	/**

	 * Metodo run

	 * @throws IOException

	 * @throws SAXException

	 * @throws ParserConfigurationException

	 */

	public void run() throws ParserConfigurationException, SAXException, IOException {



		Scanner sc = new Scanner(System.in);

		boolean fin = false;

		long startTime = 0;
		long endTime = 0;
		long duration = 0;


		while(!fin)

		{

			menu();



			int option = sc.nextInt();

			switch(option)

			{

			case 1:


				if(!carga) {

					System.out.println("Se cargarán los datos del mapa de la ciudad de Washington.");


					startTime = System.currentTimeMillis();	
					cargarGrafoJson();
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

					metodos = new MethodsManager(grafoD, newGraph);

					System.out.println("¡Se cargaron los datos!");


				} else {

					System.out.println("Ya se habían cargado los datos.");

				}




				break;

			case 2:


				if(carga != true) {


					System.out.println("Los datos no se han cargado para versualizar el grafo en el mapa.");

				} else {

					metodos = new MethodsManager(grafoD, newGraph);

					System.out.println("Ingrese El id del primer vertice (Ej. 901839): ");
					String idVertice1 = sc.next();
					System.out.println("Ingrese El id del segundo vertice (Ej. 901839): ");
					String idVertice2 = sc.next();


					startTime = System.currentTimeMillis();
					metodos.caminoCostoMinimoA1(idVertice1, idVertice2);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

					//tamp = new Mapa("Grafo Cargado", newGraph);

				}

				break;


			case 3:

				if(carga != true) {

					System.out.println("Los datos del grafo no se han cargado");

				} else {

					System.out.println("2A. Consultar los N v�rtices con mayor n�mero de infracciones. Ingrese el valor de N: ");
					int n = sc.nextInt();

					startTime = System.currentTimeMillis();
					metodos.mayorNumeroVerticesA2(n, vArray);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

				}


				break;

			case 4:

				System.out.println("Ingrese El id del primer vertice (Ej. 6389854777): ");
				long idVertice1 = sc.nextLong();
				System.out.println("Ingrese El id del segundo vertice (Ej. 63897274): ");
				long idVertice2 = sc.nextLong();

				startTime = System.currentTimeMillis();
				metodos.caminoMasCorto(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

				break;

			case 5:

				double lonMin;
				double lonMax;
				System.out.println("Ingrese la longitud minima (Ej. -87,806): ");
				lonMin = sc.nextDouble();
				System.out.println("Ingrese la longitud m�xima (Ej. -87,806): ");
				lonMax = sc.nextDouble();

				System.out.println("Ingrese la latitud minima (Ej. 44,806): ");
				double latMin = sc.nextDouble();
				System.out.println("Ingrese la latitud m�xima (Ej. 44,806): ");
				double latMax = sc.nextDouble();

				System.out.println("Ingrese el n�mero de columnas");
				int columnas = sc.nextInt();
				System.out.println("Ingrese el n�mero de filas");
				int filas = sc.nextInt();


				startTime = System.currentTimeMillis();
				metodos.cuadriculaRegular(lonMin,lonMax,latMin,latMax,columnas,filas);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");
				break;

			case 6:

				startTime = System.currentTimeMillis();
				metodos.arbolExpansionMinima();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");
				break;

			case 7:
				startTime = System.currentTimeMillis();
				metodos.arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");
				break;

			case 8:

				startTime = System.currentTimeMillis();
				metodos.caminoCostoMinimo2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

				break;

			case 9:

				System.out.println("Ingrese El id del primer vertice (Ej. 901839): ");
				String idVertice12 = sc.next();
				System.out.println("Ingrese El id del segundo vertice (Ej. 901839): ");
				String idVertice22 = sc.next();

				startTime = System.currentTimeMillis();
				metodos.caminoMasCortoC4(idVertice12, idVertice22);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Tiempo del requerimiento: " + duration + " milisegundos");

				break;

			case 10:
				fin=true;
				sc.close();

				break;

			}

		}

	}





}

