package main;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import Controller.Controller;

public class MVC {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{
		Controller controler = new Controller();
		controler.run();
	}

}
