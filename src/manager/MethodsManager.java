package manager;

import java.util.ArrayList;
import java.util.Iterator;

import com.sun.javafx.geom.Edge;
import com.teamdev.jxmaps.ag;

import Controller.Mapa;
import model.data_structures.Arco;
import model.data_structures.Bag;
import model.data_structures.Graph;
import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.Vertice;
import model.util.Sort;
import shortestPaths.BFS;
import shortestPaths.ConectedComponent;
import shortestPaths.Dijkstra2;
import shortestPaths.DijkstraSP;
import shortestPaths.EdgeWeightedGraph;
import shortestPaths.Kruskal;
import shortestPaths.KruskalMST;
import shortestPaths.PrimMST;


public class MethodsManager 
{
	@SuppressWarnings("rawtypes")
	Graph grafoD;
	@SuppressWarnings("rawtypes")
	Graph grafoND;
	BFS bfs;
	BFS bfs2;
	private Queue<String> vertices; 
	private Graph g7C;
	Mapa map;
	private Graph G;

	public MethodsManager(Graph pGraph, Graph pGraph2)
	{
		grafoD = pGraph;
		grafoND = pGraph2;
		g7C = new Graph();

	}



	/**
	 * Requerimiento 1A: Encontrar el camino de costo m�nimo para un viaje entre dos ubicaciones geogr�ficas.
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public void caminoCostoMinimoA1(String idVertice1, String idVertice2) {

		Graph graph1A = new Graph();

		//Se crea un algoritmo de Dijkstra 
		DijkstraSP dj = new DijkstraSP(grafoD, idVertice1);

		//Iterable<Arco> camino = dj.pathTo(idVertice2);
		Iterator<Arco> it = dj.pathTo(idVertice2).iterator();

		Bag<Arco> bagArcos = new Bag<Arco>();

		Arco actual = null ;

		if(it.hasNext()) {
			actual = it.next();
			bagArcos.add(actual);
		}

		String v1 = (String)actual.getV1();
		String v2 = (String)actual.getV2();

		Vertice ve1 = (Vertice) grafoD.getVertices().get(v1);
		Vertice ve2 = (Vertice) grafoD.getVertices().get(v2);

		String info1 = ve1.getLongitud() + "|" + ve1.getLatitud();
		String info2 = ve2.getLongitud() + "|" + ve2.getLatitud();

		String lat = (String) ve1.getLatitud();
		String lon = (String) ve1.getLongitud();

		String lat2 = (String) ve2.getLatitud();
		String lon2 = (String) ve2.getLongitud();


		//Se agrega el nuevo vertice al grafo
		graph1A.addVertex(v1, info1);
		Vertice v = (Vertice) graph1A.getVertices().get(v1);
		v.setLat(lat);
		v.setLon(lon);

		graph1A.addVertex(v2, info2);
		Vertice vv = (Vertice) graph1A.getVertices().get(v2);
		vv.setLat(lat);
		vv.setLon(lon);


		System.out.println("Id: " + ve1.getId());
		System.out.println("Latitud: " + ve1.getLatitud());
		System.out.println("Longitud: " + ve1.getLongitud() + "\n");

		System.out.println("Id: " + ve2.getId());
		System.out.println("Latitud: " + ve2.getLatitud());
		System.out.println("Longitud: " + ve2.getLongitud() + "\n");


		int cont = 0;

		while(it.hasNext()) {

			actual = it.next();
			bagArcos.add(actual);

			cont+= (Integer) actual.getInfo();

			v2 = (String)actual.getV2();
			ve2 = (Vertice) grafoD.getVertices().get(v2);


			String infoAct = ve2.getLongitud()+"|"+ve2.getLatitud();

			graph1A.addVertex(v2, infoAct);
			Vertice ver = (Vertice) graph1A.getVertices().get(v2);
			ver.setLat(ve2.getLatitud());
			ver.setLon(ve2.getLongitud());


			System.out.println("Id: " + ve2.getId());
			System.out.println("Latitud: " + ve2.getLatitud());
			System.out.println("Longitud: " + ve2.getLongitud() + "\n");

		}


		Iterator<Arco> itArcos = bagArcos.iterator();
		Double km = 0.0;
		while(itArcos.hasNext()) {


			Arco a = itArcos.next();
			String vIni = (String) a.getV1();
			String vFin = (String) a.getV2();

			String info = vIni + "|" + vFin;

			Arco encontrado = (Arco) grafoND.getLPArcos().get(info);

			if(encontrado != null) {

				km+= (Double) encontrado.getInfo();
				graph1A.addEdge(encontrado.getV1(), encontrado.getV2(),(Double) encontrado.getInfo());

			}


		}


		System.out.println("\nCantidad infracciones en el camino: " + cont);
		System.out.println("Distancia recorrida en km: " + km + "\n");

		//Se visualiza en el mapa
		map = new Mapa("Camino costo minimo", graph1A,1);




	}

	/**
	 * Requerimiento 2A: Determinar los n v�rtices con mayor n�mero de infracciones. Adicionalmente identificar las
	 * componentes conectadas (subgrafos) que se definan �nicamente entre estos n v�rtices
	 * @param  int n: numero de vertices con mayor numero de infracciones  
	 */
	public void mayorNumeroVerticesA2(int n, Vertice[] vArray) {

		Sort sort = new Sort();
		Vertice[] listV = vArray;
		Vertice[] aux = new Vertice[vArray.length];
		sort.ordenarMergeSort(listV, aux, 0, vArray.length-1);


		ArrayList<ConectedComponent> arrayCC = new ArrayList<ConectedComponent>();

		int inicial = 0;
		Graph g = new Graph();

		for (int i = 0; i < n-1; i++) {

			Vertice v = listV[i];

			Bag<Vertice> verticesCC = new Bag<Vertice>();
			Bag<Arco> arcosCC = new Bag<Arco>();

			verticesCC.add(v);
			//g.addVertex(v.getId(), v.getInfo());

			//Recorrer caminos
			BFS camino = new BFS(grafoND, (String) v.getId());


			for (int j = i+1; j < n; j++) {

				Vertice vertice = listV[j];


				//Se puede llegar desde el v inicial a este vertice?
				if(camino.hasPathTo((String)vertice.getId()) && vertice != v) {


					Iterator<String> itVertices = camino.pathTo((String) vertice.getId()).iterator();


					int contVertices = 0;

					String vAdjId = itVertices.next();;
					String vAdjId2 = null;
					int count = 0;

					while(itVertices.hasNext()) {

						if(count != 0) {

							vAdjId = vAdjId2;

						}
						vAdjId2 = itVertices.next();

						Vertice vAdj = (Vertice) grafoND.getVertices().get(vAdjId);

						String lat1 = (String) vAdj.getLatitud();
						String lon1 = (String) vAdj.getLongitud();

						verticesCC.add(vAdj);
						vAdj.setLat(lat1);
						g.addVertex(vAdj.getId(), vAdj.getInfo());

						Vertice ve = (Vertice) g.getVertices().get(vAdjId);
						ve.setLat(lat1);
						ve.setLon(lon1);

						//System.out.println(vAdj.getLatitud());


						Vertice vAdj2 = (Vertice) grafoND.getVertices().get(vAdjId2);
						String lat2 = (String) vAdj2.getLatitud();
						String lon2 = (String) vAdj.getLongitud();

						verticesCC.add(vAdj);
						vAdj2.setLat(lat2);
						g.addVertex(vAdj2.getId(), vAdj2.getInfo());

						Vertice ve2 = (Vertice) g.getVertices().get(vAdjId2);
						ve2.setLat(lat2);
						ve2.setLon(lon2);

						//System.out.println(vAdj2.getLatitud());


						String idArco = vAdj.getId() + "|" + vAdj2.getId();
						String idArco2 = vAdj2.getId() + "|" + vAdj.getId();


						Arco aActual = (Arco) grafoND.getLP().get(idArco);

						//System.out.println("1 "+ aActual);

						if(aActual == null) {

							aActual = (Arco) grafoND.getLP().get(idArco2);

							arcosCC.add(aActual);

						}

						g.addEdge(aActual.getV1(), aActual.getV2(), aActual.getInfo());

						//System.out.println("2 "+ aActual);

						contVertices++;

						count = 1;
					}

					ConectedComponent cc = new ConectedComponent(contVertices, verticesCC, arcosCC);
					arrayCC.add(cc);


				}


			}

		}



		for (int i = 0; i < arrayCC.size(); i++) {

			ConectedComponent actualCC = arrayCC.get(i);

		}

		for (int i = 0; i < n; i++) {

			Vertice v = listV[i];


			System.out.println("Id: " + v.getId());
			System.out.println("Lat: " + v.getLatitud());
			System.out.println("Lon: " + v.getLongitud());
			System.out.println("Infracciones: " + v.getNumInf());
			System.out.println();

		}	



		System.out.println("\nIds de los vertices de las componentes: ");
		int contCC = 0;
		for (int i = 0; i < arrayCC.size(); i++) {

			ConectedComponent actualCC = arrayCC.get(i);
			Iterator<Vertice> itBagV = actualCC.getVertices().iterator();


			while(itBagV.hasNext()) {


				Vertice actual =  itBagV.next();
				System.out.println(actual.getId());

			}


			contCC++;
		}


		System.out.println();
		System.out.println("Cantidad de componentes conectadas: " + contCC);
		System.out.println("Cantidad de vertices: " + g.getNumVertices());
		System.out.println("Cantidad de arcos: " + g.getNumArcos());
		System.out.println();

		g7C = g;
		map = new Mapa("Mayor numero vertices", g, 1);
	}



	/**
	 * Punto Punto 1B.Da el camino más corto para un viaje entre las coordenadas aleatorias de un grafo.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void caminoMasCorto(Long vInicial,Long vFinal)
	{
		Graph generado = new Graph();
		String vI= String.valueOf(vInicial);
		String vF= String.valueOf(vFinal);

		bfs = new BFS(grafoND, vI);

		double distanciaAcum=0.0;

		int dis =bfs.distTo(vF);
		System.out.println("Total de arcos: " +dis);

		System.out.println("Total de vertices: " + (dis+1) );

		System.out.println("Camino a seguir");

		Iterator<String> iter = bfs.pathTo(vF).iterator();
		int cont =0;

		String key = (String) iter.next();
		String key2 =null;



		while(iter.hasNext()) 
		{

			if(cont!=0) {
				key = key2;
			}
			key2 = (String)iter.next(); 

			Vertice v = (Vertice) grafoND.getVertices().get(key);
			Vertice v2 = (Vertice) grafoND.getVertices().get(key2);

			System.out.println();
			System.out.println("Id: "+v.getId());
			System.out.println("Lat: "+v.getLatitud());
			System.out.println("Lon: "+v.getLongitud());


			double lat1 = Double.parseDouble(v.getLatitud().toString());
			double lon1 = Double.parseDouble(v.getLongitud().toString());
			double lat2 = Double.parseDouble(v2.getLatitud().toString());
			double lon2 = Double.parseDouble(v2.getLongitud().toString());

			System.out.println();

			generado.addVertex(v.getId(), v.getInfo());
			generado.addVertex(v2.getId(), v.getInfo());

			double distancia = distance(lat1, lon1, lat2, lon2);

			Arco a = new Arco(key, key2, distancia);
			Vertice d =(Vertice)generado.getVertices().get(key);
			d.setLat(v.getLatitud());
			d.setLon(v.getLongitud());
			Vertice d2 =(Vertice)generado.getVertices().get(key2);
			d2.setLat(v2.getLatitud());
			d2.setLon(v2.getLongitud());

			generado.addEdge(a.getV1(),a.getV2(),a.getInfo());

			distanciaAcum+=distancia;
			System.out.println("Distancia ente vertice de arriba y abajo: "+ distancia+" Km");
			cont=1;

		}

		Vertice v =(Vertice)generado.getVertices().get(vF);

		System.out.println();
		System.out.println("Id: "+v.getId());
		System.out.println("Lat: "+v.getLatitud());
		System.out.println("Lon: "+v.getLongitud());

		map = new Mapa("Camino costo minimo",generado,1 );

		System.out.println("Total de vertices(sin incluir vertice final): " +dis);

		System.out.println("Total de vertices: " + (dis+1) );

		System.out.println("Distancia entre punto inicial y final: " + distanciaAcum+" Km" );

	}

	/**
	 * Punto Punto 2B. A partir de las coordenadas maximas y minimas de la cuidad, define una cuadricula regular de N columnas x M filas
	 * con N y M >= 2 
	 */	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void cuadriculaRegular(double lonMin, double lonMax, double latMin, double latMax, int columnas,int filas) {

		G = new Graph<>();


		double distX = lonMax - lonMin;
		double distY = latMax - latMin;
		double numParticionX = (distX/filas);
		double numParticionY = (distY/columnas);

		vertices = new Queue<String>();


		for (double i = lonMin; i <= lonMax; i+= numParticionX) {
			for (double j = latMin; j <= latMax; j+=numParticionY) {

				double cerca = Double.MAX_VALUE;
				String llaveCerca ="";
				Iterator<String> it = grafoND.idVertices().iterator();
				String key;
				while (it.hasNext()) 
				{
					key = it.next();
					Vertice v = (Vertice)grafoND.getVertices().get(key);
					double lat = Double.parseDouble(v.getLatitud().toString());
					double lon = Double.parseDouble(v.getLongitud().toString());
					double distancia = distance(j, i, lat, lon);
					if(distancia<cerca)
					{
						cerca = distancia;
						llaveCerca = key;
					}

				}
				Vertice v2 = (Vertice)grafoND.getVertices().get(llaveCerca);
				System.out.println();
				System.out.println("Id vertice: " + v2.getId());
				System.out.println("Lat vertice: " + v2.getLatitud());
				System.out.println("Lon vertice: " + v2.getLongitud());

				G.addVertex(llaveCerca, v2.getInfo());
				Vertice v3 = (Vertice)G.getVertices().get(v2.getId());
				v3.setLat(v2.getLatitud());
				v3.setLon(v2.getLongitud());
				vertices.enqueue(llaveCerca);
			}

		}
		System.out.println("\n Numero de vertices del grafo: "+ vertices.size() +"\n");

		map = new Mapa("Matriz", G, 2);

	}

	/**
	 * Punto 1C. Calcular el MST usando Kruskal y el resultado del punto 2A.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public void arbolExpansionMinima()
	{
		Kruskal pmst = new Kruskal(g7C);

		Iterator<Arco> arcos = pmst.edges().iterator();
		Graph g = new Graph();
		double km = 0.0;

		while(arcos.hasNext()) {

			Arco a = arcos.next();

			String v1 = (String) a.getV1();
			String v2 = (String) a.getV2();

			Vertice ve1 = (Vertice) grafoND.getVertices().get(v1);

			String lat1 = (String) ve1.getLatitud();
			String lon1 = (String) ve1.getLongitud();
			g.addVertex(v1, 2+"|"+3);

			Vertice c1 = (Vertice) g.getVertices().get(v1);
			c1.setLat(lat1);
			c1.setLon(lon1);


			Vertice ve2 = (Vertice) grafoND.getVertices().get(v2);

			String lat2 = (String) ve2.getLatitud();
			String lon2 = (String) ve2.getLongitud();
			g.addVertex(v2, 3+"|"+2);

			Vertice c2 = (Vertice) g.getVertices().get(v2);
			c2.setLat(lat2);
			c2.setLon(lon2);

			g.addEdge(v1, v2, a.getInfo());
			km += (Double) a.getInfo();

		}


		Iterator<String> itVe = g.getVertices().keys().iterator();
		int contInf = 0;
		System.out.println("Id vertices: ");
		while(itVe.hasNext()) {

			String idA = itVe.next();
			Vertice ve = (Vertice) g.getVertices().get(idA);

			System.out.println(ve.getId());

			contInf += ve.getNumInf(); 

		}

		System.out.println();
		System.out.println("Cantidad infracciones: " + contInf);
		System.out.println("Distancia recorrida en km: " + km);
		System.out.println();

		map = new Mapa("PRIM", g, 1);

	}

	/**
	 *  Punto 3C. Calcular caminos de costo mínimo con Dijkstra y el resultado del punto 2B.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void caminoCostoMinimo2()
	{
		BFS bfs = new BFS(grafoND, vertices.darPrimero().darElemento());

		while(!vertices.isEmpty()) {
			Iterator<String> ultimo = bfs.pathTo(vertices.dequeue()).iterator();

			int cont =0;

			String key = (String) ultimo.next();
			String key2 =null;



			while(ultimo.hasNext()) 
			{

				if(cont!=0) {
					key = key2;
				}
				key2 = (String)ultimo.next(); 

				Vertice v = (Vertice)grafoND.getVertices().get(key);
				G.addVertex(v.getId(), v.getInfo());
				Vertice ad = (Vertice)G.getVertices().get(v.getId());
				ad.setLat(v.getLatitud());
				ad.setLon(v.getLongitud());

				Vertice v2 = (Vertice)grafoND.getVertices().get(key2);
				G.addVertex(v2.getId(), v2.getInfo());
				Vertice ad2 = (Vertice)G.getVertices().get(v2.getId());
				ad2.setLat(v2.getLatitud());
				ad2.setLon(v2.getLongitud());


				double distance = distance(Double.parseDouble((String)v.getLatitud()), Double.parseDouble((String)v.getLongitud()), Double.parseDouble((String)v2.getLatitud()), Double.parseDouble((String)v2.getLongitud()));

				System.out.println("De "+ v.getId()+ " a "+ v2.getId()+", Distancia: " + distance +" Km");

				G.addEdge(v.getId(), v2.getId(), distance);


				cont=1;

			}
		}

		System.out.println(G.getNumArcos());
		System.out.println(G.getNumVertices());

		map = new  Mapa("8C", G, 1);

	}



	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2C: Calcular un �rbol de expansi�n m�nima (MST) con criterio distancia, utilizando el algoritmo de Prim. (REQ 2C)
	 */
	public void arbolMSTPrimC2() {

		PrimMST pmst = new PrimMST(g7C);

		Iterator<Arco> arcos = pmst.edges().iterator();
		Graph g = new Graph();
		double km = 0.0;

		while(arcos.hasNext()) {

			Arco a = arcos.next();

			String v1 = (String) a.getV1();
			String v2 = (String) a.getV2();

			Vertice ve1 = (Vertice) grafoND.getVertices().get(v1);

			String lat1 = (String) ve1.getLatitud();
			String lon1 = (String) ve1.getLongitud();
			g.addVertex(v1, 2+"|"+3);

			Vertice c1 = (Vertice) g.getVertices().get(v1);
			c1.setLat(lat1);
			c1.setLon(lon1);


			Vertice ve2 = (Vertice) grafoND.getVertices().get(v2);

			String lat2 = (String) ve2.getLatitud();
			String lon2 = (String) ve2.getLongitud();
			g.addVertex(v2, 3+"|"+2);

			Vertice c2 = (Vertice) g.getVertices().get(v2);
			c2.setLat(lat2);
			c2.setLon(lon2);

			g.addEdge(v1, v2, a.getInfo());
			km += (Double) a.getInfo();

		}


		Iterator<String> itVe = g.getVertices().keys().iterator();
		int contInf = 0;
		System.out.println("Id vertices: ");
		while(itVe.hasNext()) {

			String idA = itVe.next();
			Vertice ve = (Vertice) g.getVertices().get(idA);

			System.out.println(ve.getId());

			contInf += ve.getNumInf(); 

		}

		System.out.println();
		System.out.println("Cantidad infracciones: " + contInf);
		System.out.println("Distancia recorrida en km: " + km);
		System.out.println();

		map = new Mapa("PRIM", g, 1);

	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 4C:Encontrar el camino m�s corto para un viaje entre dos ubicaciones geogr�ficas escogidas aleatoriamente al interior del grafo.
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void caminoMasCortoC4(String idVertice1, String idVertice2) {
		// TODO Auto-generated method stub

		Graph g = new Graph();

		Dijkstra2 dj = new Dijkstra2(grafoD, idVertice1);
		Iterator<Arco> itArcos = dj.pathTo(idVertice2).iterator();
		int contInfracciones = 0;
		double km = 0.0;

		Arco aActual = itArcos.next();
		String v1 = (String) aActual.getV1();
		String v2 = (String) aActual.getV2();

		String info = v1+"|"+v2;
		String info2 = v2+"|"+v1;

		Arco a2 = (Arco) grafoND.getLP().get(info);

		if(a2 == null) {

			a2 = (Arco) grafoND.getLP().get(info2);

		}

		g.addEdge(a2.getV1(), a2.getV2(), a2.getInfo());



		g.addVertex(v1, 2+"|"+3);
		g.addVertex(v2, 3+"|"+3);


		g.addEdge(a2.getV1(), a2.getV2(), a2.getInfo());

		Vertice ve1 = (Vertice) g.getVertices().get(v1);

		Vertice lat1V = (Vertice) grafoND.getVertices().get(v1);
		String lat1 = (String) lat1V.getLatitud();
		String lon1 = (String) lat1V.getLongitud();

		ve1.setLat(lat1);
		ve1.setLon(lon1);

		Vertice ve2 = (Vertice) g.getVertices().get(v2);

		Vertice lat2V = (Vertice) grafoND.getVertices().get(v2);
		String lat2= (String) lat2V.getLatitud();
		String lon2 = (String) lat2V.getLongitud();



		ve2.setLat(lat2);
		ve2.setLon(lon2);


		km += (Double) a2.getInfo();

		contInfracciones+= (Integer) aActual.getInfo();

		System.out.println(v1);
		System.out.println(v2);

		while(itArcos.hasNext()) {

			aActual = itArcos.next();

			v2 = (String) aActual.getV2();

			g.addVertex(v2, 2+"|"+5);



			Vertice ve3 = (Vertice) g.getVertices().get(v2);

			Vertice lat3V = (Vertice) grafoND.getVertices().get(v2);
			String lat3 = (String) lat3V.getLatitud();
			String lon3 = (String) lat3V.getLongitud();

			ve3.setLat(lat3);
			ve3.setLon(lon3);

			info = aActual.getV1()+"|"+v2;
			info2 = v2+"|"+aActual.getV1();


			a2 = (Arco) grafoND.getLP().get(info);

			if(a2 == null) {

				a2 = (Arco) grafoND.getLP().get(info2);

			}

			g.addEdge(a2.getV1(), a2.getV2(), a2.getInfo());

			km += (Double) a2.getInfo();

			g.addEdge(a2.getV1(), a2.getV2(), a2.getInfo());

			contInfracciones+= (Integer) aActual.getInfo();


			System.out.println(v2);

		}

		System.out.println();
		System.out.println("Cantidad infracciones: "+contInfracciones);
		System.out.println("Distancia en km: "+km);


		//		Iterator<String> itString = g.getVertices().keys().iterator();
		//		while(itString.hasNext()) {
		//			
		//			String actS = itString.next();
		//			
		//			Vertice ve = (Vertice) g.getVertices().get(actS);
		//			
		//			System.out.println(ve.getLatitud());
		//			System.out.println(ve.getLongitud());
		//			
		//		}
		map = new Mapa("Camino corto", g, 1);


	}

	//Distancia haversiana
	private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

	public static double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}


}
